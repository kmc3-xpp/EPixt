import re
import socket
import sys
import yaml
from emmi.app import IocApplication
from Pixtend import Pixtend
from util import parseDcl

import logging

'''
Eigentlich ist der Plan folgender:
  - Pixtend hat Kanäle (GPIO 1, 2, 3... Analog in 1, 2, 3...)
  - ...der Mensch hat aber "Funktionen" ("Pumpe", "Temperatur", "Ventil"...)
    (Device Channel Label?)
  - 
'''

def ptype2validator(kind, type):
    '''
    
    Args:
        kind (str):
        type (str):

    Returns:
        dict:
    '''
    if kind == 'switch':
        return {
            'actor':{
                'values':{ 
                    'ON': True,
                    True: True, 
                    1: True,
                    'OFF': False, 
                    False: False, 
                    0: False
                }
            },
            'signal':{
                'values':{ 
                    'ON': 'ON',
                    True: 'ON', 
                    1: 'ON',
                    'OFF': 'OFF', 
                    False: 'OFF', 
                    0: 'OFF'
                }
            }
        }[type]
    else:
        return {
            'analog': {},
            'integer': {}
        }[kind]

def port2eapi(name, channelSpec, globalspec):
    ''' Creates an EMMI Export-API dictionary from a EPixt channel-publish dict item
    
    Args:
        name (str): Function Name that will be exported. Supported are:
            relay{n}
            gpio{n}
            digital_{out/in}{n}
            analog_{out/in}{n}
            analog_{out/in}{n}_raw

        channelSpec (dict):EPixt channel-publish dict.

    Returns:
        dict: Full EMMI Export-API dictionary.
    '''

    #schema = Schema()
    #data = schema.validate(portSpec)

    ptype, pdir, pid = parseDcl(name) # format: "{ptype}_{pdir}{pid}_{ptype_suffix}"

    ptype2record = {
        'in': 'signal',
        'out': 'actor',
        'relay': 'actor'
    }
    
    ptype2kind = {
        'digital': 'switch',
        'relay': 'switch',
        'gpio': 'switch',
        'analog': 'analog',
        'analog_raw': 'integer'
    }

    epicSpec = {}

    epicSpec['suffix'] = channelSpec['label']
    epicSpec['name'] = name
    epicSpec['kind'] = ptype2kind[ptype]

    if ptype == 'gpio':
        type = globalspec[name]
    else:
        type = ptype2record[pdir]
    
    epicSpec['validator'] = ptype2validator(ptype2kind[ptype], type)

    return {
        'recordType': type,
        type: epicSpec
    }

def setupGlobalSettings(name, spec, pix):
    '''
    
    Args:
        kind (str):
        spec (str):
        pix (Pixtend):

    Returns:
        dict:
    '''
    type = {
        'signal': 0,
        'actor': 1}
    
    s = re.split(r'(\d+)', name)
    if s[0] == 'gpio':
        pix.setGpio(int(s[1]), type[spec])
    elif s[0] == 'pullups':
        pix.p.gpio_pullups_enable = bool(spec)


def main(argv):
    with open(argv[0] if len(argv) > 0 else './config.yaml', 'r') as file:
        data = yaml.safe_load(file)
    p = Pixtend(data)

    app = IocApplication(prefix = data['prefix'] if 'prefix' in data else socket.gethostname(), setupIoc=True)

    for k,v in data["global"].items():
        setupGlobalSettings(k, v, p)

    for k,v in data["channels"].items():
        app.exportObject(p.p, port2eapi(k, v, data["global"]))
    
    app.runIoc()

if __name__ == "__main__":
    main(sys.argv[1:])