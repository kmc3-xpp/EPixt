from util import parseDcl

class DemoPixtend:

    def __init__(self, type):
        self.values = {}
        if type == 's':
            self.parameters = {
                'digital': [4, 8],
                'relay': [4, 4],
                'gpio': [4, 4],
                'analog_raw': [2, 2]
            }
        else:
            self.parameters = {
                'digital': [12, 16],
                'relay': [4, 4],
                'gpio': [4, 4],
                'analog_raw': [2, 6]
            }
            
    def __getattr__(self, attr):
        if attr == 'values' or attr == 'parameters':
            return object.__getattribute__(self, attr)
        else:
            ptype, pdir, pid = parseDcl(attr)
            if ptype in self.parameters and self.parameters[ptype][1] > int(pid):
                return self.values.get(ptype + pid, 0) 
            elif ptype == 'analog' and self.parameters['analog_raw'][1] > int(pid):
                return self.values.get(ptype + pid, 0) * (10.0 / 1024)

    def __setattr__(self, attr, value):
        if attr == 'values' or attr == 'parameters':
            object.__setattr__(self, attr, value)
        else:
            ptype, pdir, pid = parseDcl(attr)
            if ptype in self.parameters and self.parameters[ptype][0] > int(pid):
                self.values[ptype + pid]  = value
            elif ptype == 'analog' and self.parameters['analog_raw'][1] > int(pid):
                self.values[ptype + pid]  = value * (1024 / 10.0)