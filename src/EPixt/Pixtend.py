import sys
import time

from DemoPixtend import DemoPixtend

class Pixtend:

    def __init__(self, settings):
        if settings.get('demo', False):
            self.p = DemoPixtend(settings['type'])
        elif settings['type'] == 's':
            from pixtendv2s import PiXtendV2S
            self.p = PiXtendV2S()
        elif settings['type'] == 'l':
            from pixtendv2l import PiXtendV2L
            self.p = PiXtendV2L()
        else:
            sys.exit('Invalid device type')

    def __exit__(self):
        self.close()

    def setGpio(self, id, val):
        self.p._gpio_config_set(id, id + 4, val)

    def close(self):
        time.sleep(0.25)
        self.p.relay0 = False
        self.p.digital_out0 = False
        time.sleep(0.5)
        self.p.close()
        time.sleep(0.25)
