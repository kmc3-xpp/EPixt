import pytest
import EPixt
import util
import socket
import yaml
import time
import asyncio
from emmi.app import IocApplication
import Pixtend

def test_ptype2validator():

    assert EPixt.ptype2validator('analog', 'actor') == {}

    assert EPixt.ptype2validator('integer', 'actor') == {}

    assert EPixt.ptype2validator('switch', 'actor')['values']['ON'] == True

    assert EPixt.ptype2validator('switch', 'signal')['values'][False] == 'OFF'
    
def test_parseDcl():

    assert util.parseDcl('digital_out0') == ('digital', 'out', '0')

    assert util.parseDcl('relay6') == ('relay', 'relay', '6')

    assert util.parseDcl('analog_in3_raw') == ('analog_raw', 'in', '3')

@pytest.mark.asyncio
async def test_app_async_defaultioc():

    with open('./tests/demo.yaml', 'r') as file:
        data = yaml.safe_load(file)
    p = Pixtend.Pixtend(data)

    app = IocApplication(prefix = data['prefix'], setupIoc=True)
    
    for k,v in data["channels"].items():
        app.exportObject(p.p, EPixt.port2eapi(k, v, data["global"]))
    
    app.startIoc()

    from caproto.sync import client

    tstart = time.time()
    while time.time()-tstart < 3.0:
        print (client.read(data['prefix']+":heartbeat").data)
        await asyncio.sleep(0.5)

    app.stopIoc()
